// import 'package:first_navigator/models/register_data_model.dart';
// import 'package:first_navigator/pages/register.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:navigator_p/pages/register.dart';
import '../models/register_data_model.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late TextEditingController controllerUsername = TextEditingController();
  late TextEditingController controllerEmail = TextEditingController();
  late TextEditingController controllerPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
        titleTextStyle: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(38.0),
            child: Column(
              children: [
                Image.asset(
                  'images/login1.png',
                  height: 100,
                ),
                // Username
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: TextFormField(
                    controller: controllerUsername,
                    style: const TextStyle(fontSize: 25),
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      suffixIcon: Icon(
                        Icons.supervised_user_circle,
                        size: 40,
                      ),
                      labelText: 'Enter Your Username',
                      labelStyle: TextStyle(fontSize: 15),
                    ),
                  ),
                ),

                // Email
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: TextFormField(
                    controller: controllerEmail,
                    style: const TextStyle(fontSize: 25),
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      suffixIcon: Icon(
                        Icons.email,
                        size: 35,
                      ),
                      labelText: 'Enter Your Email Address',
                      labelStyle: TextStyle(fontSize: 15),
                    ),
                  ),
                ),

                // Password
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: TextFormField(
                    controller: controllerPassword,
                    obscureText: true,
                    style: const TextStyle(fontSize: 25),
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      suffixIcon: Icon(
                        Icons.password,
                        size: 35,
                      ),
                      labelText: 'Enter Your Password',
                      labelStyle: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  // Clear button
                  child: const Text('Clear'),
                  onPressed: () {
                    controllerUsername.clear();
                    controllerEmail.clear();
                    controllerPassword.clear();
                    if (kDebugMode) {
                      print('Username => ${controllerUsername.text}\nEmail => ${controllerEmail.text}\nPassword => ${controllerPassword.text}\n');
                    }
                  },
                ),
                ElevatedButton(
                  // Register button
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white, backgroundColor: Colors.blue.shade300,
                  ),
                  child: const Text('Register'),
                  onPressed: () async {
                    final result = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RegisterPage(
                          regDataModel: RegisterDataModel(
                              controllerUsername.text,
                              controllerEmail.text,
                              controllerPassword.text
                          )
                      )),
                    );
                    if (!mounted) return;
                    ScaffoldMessenger.of(context)
                      ..removeCurrentSnackBar()
                      ..showSnackBar(SnackBar(content: Text('$result')));
                    if (kDebugMode) {
                      print('Username => ${controllerUsername.text}\nEmail => ${controllerEmail.text}\nPassword => ${controllerPassword.text}\n');
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(Icons.account_circle_outlined, size: 40,),
                    const Text('\nConfirm your info.', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, backgroundColor: Colors.white),),
                    const Divider(thickness: 4,),
                    Text('\nUsername: ${controllerUsername.text}'),
                    Text('Email: ${controllerEmail.text}'),
                    Text('Password: ${controllerPassword.text}'),
                  ],
                ),
              );
            },
          );
        },
        tooltip: 'Confirm Info',
        child: const Icon(Icons.add_alert),
      ),
    );
  }
}
