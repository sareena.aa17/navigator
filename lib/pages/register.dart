import 'package:flutter/material.dart';
import '../models/register_data_model.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage ({super.key, required this.regDataModel});

  final RegisterDataModel regDataModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Welcome'),
        centerTitle: true,
        titleTextStyle: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
      ),
      backgroundColor: Colors.white24,
      body: Container(
        alignment: Alignment.center,
        child: Center(
          child: SizedBox(
            width: 480,
            height: 550,
            child: Card(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'images/welcome6.jpg',
                      height: 150,
                    ),
                    const Text('\nWELCOME', style: TextStyle(fontSize: 55, color:Colors.blue, fontWeight: FontWeight.bold,
                        shadows: [
                          Shadow(
                            color: Colors.blue,
                            offset: Offset(1.0, 1.0)
                          ),
                          Shadow(
                            color: Colors.white,
                            offset: Offset(1.0, 1.0),
                          ),
                        ]),),
                    Text(regDataModel.reportUsername, style: const TextStyle(fontSize: 42, color: Colors.black, fontWeight: FontWeight.bold),),
                    Text('\nEmail : ${regDataModel.reportEmail}', style: const TextStyle(fontSize: 20, color: Colors.black),),
                    Text('Password : ${regDataModel.reportPassword}\n', style: const TextStyle(fontSize: 20, color: Colors.black),),
                    const Divider(thickness: 2,),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          foregroundColor: Colors.white, backgroundColor: Colors.blue.shade300,
                        ),
                        child: const Text('BACK'),
                        onPressed: () {
                          Navigator.pop(context, "Hello, ${regDataModel.reportUsername}!");
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}